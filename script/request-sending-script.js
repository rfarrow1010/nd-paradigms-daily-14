console.log('now sending specified request');

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = sendRequest;

function sendRequest() {
    // get value of select-server-address
    var address = document.getElementById('select-server-address').value;

    // get value of input-port-number
    var port = document.getElementById('input-port-number').value;

    // get value of radio (radio-get, radio-put, etc)
    var radio = '';
    if (document.getElementById('radio-get').checked) {
        radio = 'GET';
    }
    else if (document.getElementById('radio-put').checked) {
        radio = 'PUT';
    }
    else if (document.getElementById('radio-post').checked) {
        radio = 'POST';
    }
    else if (document.getElementById('radio-delete').checked) {
        radio = 'DELETE';
    }

    // get value of input-key
    var key = document.getElementById('input-key').value;

    // get value of use-key (checkbox for using input-key)
    var usingKey = document.getElementById('checkbox-use-key').checked;

    // get value of use-message-body (checkbox for using text-message-body)
    var usingBody = document.getElementById('checkbox-use-message').checked;

    // get value of text-message-body
    var message = document.getElementById('text-message-body').value;

    // send specified request to specified server
    console.log('begin REST API call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = address + ':' + port + '/movies/';

    if (usingKey) {
        url = url + key;
    }

    // 2 - associates request attributes with xhr
    if (radio == 'GET') {
        xhr.open("GET", url, true); 
    }
    else if (radio == 'PUT') {
        xhr.open("PUT", url, true); 
    }
    else if (radio == 'POST') {
        xhr.open("POST", url, true); 
    }
    else if (radio == 'DELETE') {
        xhr.open("DELETE", url, true); 
    }

    console.log('The request will be made to: ' + url);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // write result to answer-label
        writeResult(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    if (usingBody) {
        xhr.send(message);
    }
    else {
        xhr.send(null);
    }
}

function writeResult(text) {
    var response_json = JSON.parse(text);
    // update answer-label
    var label = document.getElementById("answer-label");

    if(response_json['result'] == 'error'){
        label.innerHTML = "Sorry, your request failed. Here's why: " + response_json['message'];
    } else{
        label.innerHTML = text;
    }
}

// this is Kumar's sample code in case any of it comes in handy
/*
function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("name-text").value;
    console.log('Number you entered is ' + name);
    makeNetworkCallToAgeApi(name);

} // end of get form info

function makeNetworkCallToAgeApi(name){
    console.log('entered make nw call' + name);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.agify.io/?name=" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateAgeWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateAgeWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json['age'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  name + ', your age is ' + response_json['age'];
        var age = parseInt(response_json['age']);
        makeNetworkCallToNumbers(age);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToNumbers(age){
    console.log('entered make nw call' + age);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://numbersapi.com/" + age;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateTriviaWithResponse(age, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateTriviaWithResponse(age, response_text){
    // update a label
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = response_text;

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(response_text); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
*/